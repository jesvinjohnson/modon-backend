var express = require('express');
var path = require('path');
var axios = require('axios');

var app = express();

var server = require('http').createServer(app);
var io = require('socket.io')(server);

var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

var http = require('http');
var server = http.createServer(app);
server.setTimeout(10*60*1000);

var io = io.listen(server);

server.listen(3000, function () {
  console.log('Listening on port 3000');
  console.log('Cmd - npm start ');
});


io.on('connection', function(client) {
    console.log('Client connected...');

    client.on('join', function(data) {
        console.log(data);
    });  
    client.on('broad', function(data) {
        console.log(data);
    });

    client.on('messages', function(data) {
        client.emit('broad', data);
        client.broadcast.emit('broad',data);
    });



});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'node_modules')));

//const PORT = 3000;

var router = express.Router();




// -- API calls --

router.get('/', function (req, res) {
  	res.render('index');
});

router.get('/pages/section2', function (req, res) {
  res.render('pages/section2');
});

router.get('/pages/section3', function (req, res) {
  res.render('pages/section3');
});

router.get('/pages/section4', function (req, res) {
  res.render('pages/section4');
});

router.get('/pages/section5', function (req, res) {
  res.render('pages/section5');
});

router.get('/pages/section6', function (req, res) {
	var id = req.query.id;

	axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
        IndustrialCityID: id
    }).then(function (response) {

    	res.render('pages/section6', {data : response.data, cityid: id });

    }).catch(function (error) {
    	
        console.log('error');
        console.log(error);
    });
	
  	
});

router.get('/pages/section7', function (req, res) {
  res.render('pages/section7');
});

router.get('/pages/section8', function (req, res) {
  res.render('pages/section8');
});

router.get('/pages/section9', function (req, res) {
  res.render('pages/section9');
});

router.get('/pages/section10', function (req, res) {
	res.render('pages/section10');
  });


// ---IPAD section

router.get('/screen', function (req, res) {
  	res.render('index-ipad');
});

router.get('/pages-ipad/section2', function (req, res) {
	io.emit('broadML','mainlogo');
  	res.render('pages-ipad/section2');
});

router.get('/pages-ipad/section3', function (req, res) {
	io.emit('broadML','langEng');
  	res.render('pages-ipad/section3');
});

router.get('/pages-ipad/section4', function (req, res) {
	io.emit('broadML','skipbtn');
  	res.render('pages-ipad/section4');
});

router.post('/videobutton', function (req, res) {
	io.emit('broadML','videobutton');
	return res.json({ data : 'success'});
});

router.get('/videobutton2', function (req, res) {
	io.emit('broadML','videobutton2');
	return res.json({ data : 'success'});
});

router.post('/stateAnimate', function (req, res) {

	io.emit('broadML',req.body.stname);
	return res.json({ data : 'success'});
});

router.get('/leftMenuSlide', function (req, res) {

	io.emit('broadML','leftMenuSlide');
	return res.json({ data : 'success'});
});

router.get('/check1', function (req, res) {
io.emit('broadML','check1');
return res.json({ data : 'success'});
});

router.get('/check2', function (req, res) {
io.emit('broadML','check2');
return res.json({ data : 'success'});
});


router.get('/check3', function (req, res) {
io.emit('broadML','check3');
return res.json({ data : 'success'});
});

router.get('/uncheck1', function (req, res) {
io.emit('broadML','uncheck1');
return res.json({ data : 'success'});
});

router.get('/uncheck2', function (req, res) {
io.emit('broadML','uncheck2');
return res.json({ data : 'success'});
});

router.get('/uncheck3', function (req, res) {
io.emit('broadML','uncheck3');
return res.json({ data : 'success'});
});

router.get('/onLoad', function (req, res) {

io.emit('broadML','onLoad');
return res.json({ data : 'success'});
});

router.get('/mainReset', function (req, res) {

	io.emit('broadML','mainReset');
	return res.json({ data : 'success'});
});

router.get('/mainMap', function (req, res) {
	io.emit('broadML','mainMap');
	return res.json({ data : 'success'});
});

router.get('/pages-ipad/section5', function (req, res) {
	io.emit('broadML','section5');
  	res.render('pages-ipad/section5');
});

router.get('/pages-ipad/section6', function (req, res) {
	io.emit('broadML','section6');
	// res.render('pages-ipad/section6');
	var id = req.query.id;

	axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
        IndustrialCityID: id
    }).then(function (response) {

    	res.render('pages-ipad/section6', {data : response.data, cityid: id });

    }).catch(function (error) {
    	
        console.log('error');
        console.log(error);
    });
});

router.get('/pages-ipad/section7', function (req, res) {
	io.emit('broadML','section7');
  	res.render('pages-ipad/section7');
});

router.get('/pages-ipad/section8', function (req, res) {
	io.emit('broadML','section8');
  	res.render('pages-ipad/section8');
});

router.get('/pages-ipad/section9', function (req, res) {
	io.emit('broadML','section9');
  	res.render('pages-ipad/section9');
});

router.get('/pages-ipad/section10', function (req, res) {
	io.emit('broadML','section10');
  	res.render('pages-ipad/section10');
});

router.post('/fetchIndustrialData', function (req, res) { 

	temp = req.body.industryData;
	lang = req.body.industryData;
	temp2 = temp.split('_');
	name = temp2[0];
	id = temp2[1];
//console.log(temp);
	axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
        IndustrialCityID: id
    }).then(function (response) {
    	
    	//console.log(response.data);
    	if(response.data){

    		var out='<div class="image-area">'+
	            '<img src="images/gallery/riyadh_1st/DJI_0018.JPG" class="card-img-top">'+
	            '<div class="text">'+
	                '<h4>'+response.data.IndustrialCityNameEn+'</h4>'+
	                '<p class="short-desc">'+response.data.CompetitiveAdvantageEn+'</p>'+
	            '</div>'+
        	'</div>'+
	        '<ul class="list-group list-group-flush">'+
	            '<li class="list-group-item"><span class="list-title">Established Date</span><span class="list-data">'+response.data.EstablishedDate+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Total Area (m<sup>2</sup>)</span><span class="list-data">'+response.data.Area+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Number of Factories</span><span class="list-data">'+response.data.Count+' factories</span></li>'+
	        '</ul>'+
	        '<div class="card-body">'+
	            '<a id="'+response.data.IndustrialCityID+'" href="#" class="card-link">more . . .</a>'+
			'</div>'
			
			var out2='<div class="image-area">'+
	            '<img src="images/gallery/riyadh_1st/DJI_0018.JPG" class="card-img-top">'+
	            '<div class="text">'+
	                '<h4>'+response.data.IndustrialCityNameAr+'</h4>'+
	                '<p class="short-desc">'+response.data.CompetitiveAdvantageAr+'</p>'+
	            '</div>'+
        	'</div>'+
	        '<ul class="list-group list-group-flush">'+
	            '<li class="list-group-item"><span class="list-title">Established Date</span><span class="list-data">'+response.data.EstablishedDate+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Total Area (m<sup>2</sup>)</span><span class="list-data">'+response.data.Area+'</span></li>'+
	            '<li class="list-group-item"><span class="list-title">Number of Factories</span><span class="list-data">'+response.data.Count+' factories</span></li>'+
	        '</ul>'+
	        '<div class="card-body">'+
	            '<a id="'+response.data.IndustrialCityID+'" href="#" class="card-link">more . . .</a>'+
	        '</div>'
    	}

    	return res.json({data : out, data2 : out2 });


    }).catch(function (error) {
    	
        console.log('error');
        //console.log(error);
    });
});



router.post('/fetchStateData', function (req, res) {
	
	if(req.body.stateid == 1){
		// 	// ----------------------
// var cityid = [2,16,21,1,32,26,37,47,43,51,52,53,54,17];

// 		var obj1 = [];
// 		var loopc = 0;
// 		cityid.forEach(function(datau,index) {
// 			axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
// 		        IndustrialCityID: datau
// 		    }).then(function (response) {
// 		    	loopc++;
		       	
// 		       	ff = response.data;
		 
// 		       	obj1.push({ datau : ff });
// 				if(loopc === cityid.length){
// 					//console.log('gg')
// 				}
// 		    }).catch(function (error) {
// 		    	return;
// 		        console.log('error');
// 		    });
			
// 			// body...
// 		})
		
		
// 	// ----------------------
	}

	// if(req.body.stateid == 2){
	// 	var cityid = [15,6,28,3,39,29,22];

	// }

	// if(req.body.stateid == 3){
	// 	var cityid = [42,9];

	// }

	// if(req.body.stateid == 4){
	// 	var cityid = [7,25,44];

	// }

	// if(req.body.stateid == 5){
	// 	var cityid = [5,30,8,4,35,36,38,45];

	// }

	// if(req.body.stateid == 6){
	// 	var cityid = [10,46,41];

	// }

	// if(req.body.stateid == 7){
	// 	var cityid = [12];

	// }

	// if(req.body.stateid == 8){
	// 	var cityid = [13,40];

	// }

	// if(req.body.stateid == 9){
	// 	var cityid = [19,49,50,48];

	// }

	// if(req.body.stateid == 10){
	// 	var cityid = [18];

	// }

	// if(req.body.stateid == 11){
	// 	var cityid = [14];

	// }

	// if(req.body.stateid == 12){
	// 	var cityid = [56,27];

	// }

	// if(req.body.stateid == 13){
	// 	var cityid = [11,55];

	// }
	

});




// -- EO API calls --

router.get('/sample', function (req, res) {
	// ----------------------
	axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetCitiesByRegion', {
	    RegionID: 12
	}).then(function (response) {
		if(response.data){

			console.log(typeof(response.data)); //output -- string
			console.log(response.data.length); //output -- string
			citys = response.data;
			citys.forEach(function(datar){

				console.log(datar)
			})
			//console.log(typeof(JSON.parse(JSON.stringify(rr))));

		}
	}).catch(function (error) {
	    console.log(error);
	});
	// ----------------------
});

// router.get('/sample2', function (req, res) {
// 	// ----------------------
//     axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
//         IndustrialCityID: 1
//     }).then(function (response) {
//        	console.log(typeof(response.data));
//        	ff = response.data;

//        	console.log(ff.IndustrialCityNameAr);
//     }).catch(function (error) {
//         console.log(error);
//     });
// 	// ----------------------
// });

router.get('/sample3', function (req, res) {
// 	// ----------------------
var cityid = [2,16,21,1,32,26,37,47,43,51,52,53,54,17];

		var obj1 = {};

		cityid.forEach(function(datau) {
			axios.post('http://maps.modon.gov.sa/interactiveWallGIS/service.asmx/GetIndustrialInfo', {
		        IndustrialCityID: datau
		    }).then(function (response) {
		       	console.log(typeof(response.data));
		       	ff = response.data;
		       	
		       	// obj1 = { datau : ff };
		       	obj1 = Object.assign({}, obj1);
		    }).catch(function (error) {
		        console.log(error);
		    });
			
			// body...
		})
		console.log(JSON.parse(obj1));
		return obj1;
// 	// ----------------------
});

app.use('/', router);

// server.listen(PORT, function () {
//   console.log('Listening on port ' + PORT);
//   console.log('Cmd - npm start ');
// });
 
