1. Riyadh-
الرياض الثانية : Riyadh 2nd
سدير : Sudayer
الزلفي : Az-Zulfi
الرياض الأولى : Riyadh 1st
شقراء : Shaqra
الرياض الثالثة : Riyadh 3d
ضرماء : Dhurma
حوطة بني تميم : HwatetBanyTamem
رماح : Romaah
الدوادمي : Dwadmi
الافلاج : Aflaj
القويعية : AlQwaieah
السليل : AlSlalel
الخرج : Al Kharj

2. Makkah-
جدة الثانية : Jeddah 2nd
مكة المكرمة : Makkah Al-Mukarrama
جدة الثالثة : Jeddah 3rd
جدة الأولى : Jeddah 1st
رابغ : Rabigh
واحة مدن بجدة : JeddahOsais
الطائف : Taif

3. Medina-
واحة مدن بينبع : YanbueOsais
المدينة المنورة : Al-Madina Al-Munawwara

4. Buraidah-
القصيم : Al-Qassim
القصيم الثانية : Al-Qassim 2nd
واحة مدن بالقصيم : Al-QassimOsais

5. Dammam-
الدمام الثانية : Dammam 2nd
الدمام الثالثة : Dammam 3rd
الإحساء الأولى : Ahsa 1st
الدمام الأولى : Dammam 1st
حفر الباطن : Hafr Albatin
الأحساء الثانية : Al-ahsa 2nd
واحة مدن بالأحساء : AhsaOsais
الخفجي : Khafji

6. Abha-
عسير : Assir
تثليث : Tthleeth
عسير الثانية : AseerSeconed

7. Tabuk-
تبوك : Tabuk

8. Hail-
حائل : Hail
واحة مدن بحائل : HailOsais

9. Arar-
عرعر : Ar'ar
واحة مدن بعرعر : ArarOsais
عرعر الحدودية : ArarHdodia
وعد الشمال : Wa'ad AL-Shamal

10. Jazan-
جازان : Jazan

11. Najran-
نجران : Najran

12. Al Baha-
الباحة الثانية : Al Baha 2nd
الباحة : Al Baha

13. Skaka-
واحة مدن بالجوف : Al_JoufOsais
القريات : AlQuriat


