
jQuery(document).ready(function() {

    //INIT and RESET VALUES

    function init(){
        APP.selectedCities = [];
        APP.makeCheckbox = function() {
            const checkboxHeader = `
                                <div class="checkbox">
                                    <label class="form-check-label" for="check0"><span>Selected Cities</span></label>
                                </div>`;
        
            const checkbox = `
            ${APP.selectedCities.map(city => 
                            `
                            <div class="checkbox">
                                <input class="form-check-input" type="checkbox" checked value="${city.id}" id="check${city.id}">
                                <label class="form-check-label" for="check${city.id}"><span>${city.name}</span></label>    
                            </div>
                            `
                            ).join('')}
                        `;
            
            $('.dropdown.dd_selected').first().after().html('').append(checkboxHeader+checkbox);
            $('.btn_round.briefcase .badge').html(APP.selectedCities.length);
          }  
        
    }
    init();
    /* MENU SLIDING FUNCTION - SECTION 4,5 */

    jQuery("main").on("click","#showLeft", function(){

        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
        body = document.getElementById( 'section4' );
        classie.toggle( this, 'active' );
        classie.toggle( menuLeft, 'cbp-spmenu-open' );

        $.ajax({
            type: "GET",
            url: '/leftMenuSlide',
            dataType: "json",
            processdata: true,
            success: function(re) {
            },
            error: function() {

        }});
       

    });

    /* MENU SLIDING FUNCTION - SECTION 6 */

    jQuery("main").on("click","#showLeftPush", function(){
        var menuLeft = document.getElementById( 'cbp-spmenu-s1' ),
            body = document.getElementById( 'push' );
            classie.toggle( this, 'active' );
            classie.toggle( body, 'cbp-spmenu-push-toright' );
            classie.toggle( menuLeft, 'cbp-spmenu-open' );
    });

    /* ROUND BUTTON MENU TOGGLE FUNCTION */

    jQuery("main").on("click",".outer-btn", function(){
        jQuery('.inner-btn').fadeToggle();
    });

    /* POPUP OPEN FUNCTION - SECTION 5 */

    jQuery("main").on("click", function(e){
        jQuery('#section4 .map-popup').fadeOut();
        e.stopPropagation();
    });
    jQuery("main").on("click",".riyadh-img", function(e){
        e.stopPropagation();
        jQuery('#section4 .map-popup').fadeIn();
    });
    jQuery("main").on("click","#section4 .map-popup", function(e){
        e.stopPropagation();
    });

    /*VIDEO MODAL FUNCTIONS - SECTION 6 */

    jQuery("main").on("shown.bs.modal","#mainvideoModal",function(){
        
        var vid = document.getElementById("mainvideo");
        vid.play();
    });
    jQuery("main").on("hide.bs.modal","#mainvideoModal",function(){
        
        var vid = document.getElementById("mainvideo");
        vid.load();
    });

    /* MENU CLICK, MENU ACTIVE STATE, BOX SLIDE FUNCTION - SECTION 6 */

    jQuery("main").on("click",".menu-in", function(e){
        e.stopPropagation();
        if (jQuery(this).hasClass("open-box")) {
            jQuery('.icon-box').addClass('display');
            jQuery('.data-container.gallery').hide();
            jQuery('.data-container.services').show();
        }
        if (jQuery(this).hasClass("open-popup")) {
            jQuery('.data-container.services').hide();
            jQuery('.data-container.gallery').show();
        }
        var button_index = jQuery(this).parent(".form-check").index(".menu-form .form-check");
        jQuery(this).addClass("active");
        jQuery("main").find(".menu-in").not(this).removeClass("active");
        jQuery("main").find(".data-box-in:eq(" + button_index + ")").css({
            "visibility": "visible",
            "height": "auto",
            "overflow": "visible"
        });
        jQuery("main").find(".data-box-in").not(".data-box-in:eq(" + button_index + ")").css({
            "visibility": "hidden",
            "height": "0",
            "overflow": "hidden"
        });
    });

    /* GALLERY POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click",".close-gallery", function(){
        jQuery('.data-container.gallery').hide();
        jQuery('.data-container.services').show();
    });

    /* GALLERY LAYOUT FUNCTION - SECTION 6 */

    jQuery("main").on("click",".open-popup", function(){
        var $grid = jQuery('.grid').masonry({
            columnWidth: '.grid-sizer',
            gutter: '.gutter-sizer',
            itemSelector: '.grid-item',
            percentPosition: true
        });
        $grid.imagesLoaded().progress( function() {
            $grid.masonry();
        });
    });

    /* ROUND BUTTON HOME POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_home", function(e){
        e.stopPropagation();
        jQuery('.dd_home').fadeToggle();
    });

    jQuery("main").on("click", function(){
        jQuery('.dd_home').fadeOut();
    });

    /* ROUND BUTTON VIDEO PLAY FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_vdo", function(){

        $.ajax({
        type: "GET",
        url: '/videobutton2',
        dataType: "json",
        processdata: true,

        success: function(re) {
            //jQuery("main").load("pages/section3.html .english");
        },

        error: function() {

        }
      }); 

     
    });

    /* ROUND BUTTON SHOW SELECTED FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_selected", function(e){
        e.stopPropagation();
        jQuery('.dd_selected').fadeToggle();
    });

    jQuery("main").on("click",".dd_selected", function(e){
        e.stopPropagation();
    });

    jQuery("main").on("click", function(){
        jQuery('.dd_selected').fadeOut();
    });

    /* ROUND BUTTON EMAIL POPUP FUNCTION - SECTION 6 */

    jQuery("main").on("click",".btn_email", function(){
        jQuery('#formModal').modal({
            backdrop: false
        });
    });

    //COMMON SIDE BUTTONS

    jQuery("main").on("click",".dropdown.dd_home a.city-map",function(){
        console.log("logging citymap")
    });

    jQuery("main").on("click",".dropdown.dd_home a.reset",function(){

        $.ajax({
            type: "GET",
            url: '/mainReset',
            dataType: "json",
            processdata: true,

            success: function(re) {
            },

            error: function() {
            }
        });

        console.log("logging reset");
    });

    jQuery("main").on("click",".btn_round.mail",function(){
        console.log(selectedCities);
    });

    jQuery("main").on("click",".btn_round.briefcase",function(){
        console.log("logging case")
    });


    jQuery("main").on("click",".btn_round.plus",function(){
        selectedCities.push({
            name : "Riyadh 2nd Industrial City",
            id : selectedCities.length+1
        });

        const checkboxHeader = `
                        <div class="checkbox">
                            <label class="form-check-label" for="check0"><span>Selected Cities</span></label>
                        </div>`;

        const checkbox = `
        ${selectedCities.map(city =>
                        `
                        <div class="checkbox">
                            <input class="form-check-input" type="checkbox" checked value="${city.id}" id="check${city.id}">
                            <label class="form-check-label" for="check${city.id}"><span>${city.name}</span></label>
                        </div>
                        `
                        ).join('')}
                    `;

        $('.dropdown.dd_selected').first().after().html('').append(checkboxHeader+checkbox);
        $('.btn_round.briefcase .badge').html(selectedCities.length);
        // $(".checkbox input").prop("checked",true)
    });



});
