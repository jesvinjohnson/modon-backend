var APP = {}

jQuery(document).ready(function() {

    /* NAVIGATION ACROSS THE APP */

    //Load iPad and Desktop together 
    $.ajax({
      type: "GET",
      url: '/onLoad',
      dataType: "json",
      processdata: true,
      success: function(re) {
      //reloading Desktop
      }
    });
    //section 1 to section 2

    jQuery("main").on("click",".logo-center", function(){
        
        jQuery("main").load("pages-ipad/section2",function(e){
          section2('section2')
        });
        // jQuery("main").load("pages-ipad/section6 .english",function(e){
        //   section6('section6');
        // });
    });

    //section 2 to section 3


    //section 3 to section 4

    jQuery("main").on("click",".skip-btn1", function(){
        jQuery("main").load("pages-ipad/section4 .english",function(e){
          section4('section4');
        });
    });

    //section 6 to section 4

    jQuery("main").on("click",".main-map", function(){

        $.ajax({
          type: "GET",
          url: '/mainMap',
          dataType: "json",
          processdata: true,

          success: function(re) {

            jQuery("main").load("pages-ipad/section4 .english",function(e){
              section4('section4');
            });
          },

          error: function() {
          }
        });
      
    });


    /* PLAY VIDEO ON SECTION 3 - REDIRECTION TO SECTION 4 */

    jQuery("main").on("click",".play-btn1", function(){


      jQuery('.video-poster').fadeOut();
      jQuery('.video-controls').fadeOut();
      var play_vdo1 = document.getElementById("modon-vdo1");
      //play_vdo1.play();

      $.ajax({
        type: "POST",
        url: '/videobutton',
        dataType: "json",
        processdata: true,

        success: function(re) {

          jQuery(play_vdo1).on('ended',function(){
            jQuery("main").load("pages-ipad/section4 .english",function(e){
              section4('section4');
            });
          });

        },

        error: function() {

        }
      });   



    });

    // jQuery("main").on("click",".play-btn2", function(){
    //     jQuery('.video-poster').fadeOut();
    //     jQuery('.video-controls').fadeOut();
    //     var play_vdo2 = document.getElementById("modon-vdo2");
    //     play_vdo2.play();
    //     jQuery(play_vdo2).on('ended',function(){
    //         jQuery("main").load("pages-ipad/section4 .arabic");
    //     });
    // });

    //section 4 to section 5

    // jQuery("main").on("click",".saudi-img", function(){
    //     jQuery("main").load("pages-ipad/section5 .english",function(e){
    //       section5('section5');
    //     });
    // });

    //section 4 to section 6

    jQuery("main").on("click","#mapPopup.card .card-body .card-link", function(){

      let cityID = $(this).attr("id");
      jQuery("main").load("pages-ipad/section6?id="+cityID+" .english",function(e){
        section6('section6');
      });
    });

  jQuery("main").on("click","#alraidahPopup.card .card-body .card-link", function(){
    
    jQuery("main").load("pages/section9 .english",function(e){
      //section9('section9');
    });
});

  jQuery("main").on("click","#wadiPopup.card .card-body .card-link", function(){
      
    jQuery("main").load("pages/section8 .english",function(e){
      //section('section9');
    });
});

  jQuery("main").on("click","#alahsa2Popup.card .card-body .card-link", function(){
        
    jQuery("main").load("pages/section10 .english",function(e){
          var data = {
            labels: ['Food', 'Beverages', 'Textiles', 'Apparels', 'Petroleum', 'Chemicals', 'Plastic', 'Non-met. Minerals', 'Basic Metals', 'Fabricated Metal', 'Electricals', 'Other'],
            series: [[1,1,2,1,1,1,2,2,2,4,1,1]]
        };
        var options = {
            fullWidth: true,
            seriesBarDistance: 15,
            axisX: {
                offset: 90
            },
            axisY: {
                onlyInteger: true
            }
        };
        new Chartist.Bar('.ct-chart', data, options);
        });
  });

jQuery("main").on("click","#riyadhPopup.card .card-body .card-link", function(){
    jQuery("main").load("pages/section7 .english",function(e){
      section7('section7');
    });
});


    //ONLOAD CODE FOR EACH SECTION

    function section2(section) {
      console.log(section);
      let english = document.getElementById('eng_vdo')
      var animation =  lottie.loadAnimation({
        container: english,
        renderer: 'svg',
        loop: true,
        autoplay: true,
        path: './svg/english_video/data.json',
      });
      animation.addEventListener('DOMLoaded', function(){
        document.querySelector('.eng').addEventListener('click',function(){
          jQuery("main").load("pages-ipad/section3 .english",function(e){
              section3('section3');
            });
        });
        // document.querySelector('.arabic').addEventListener('click',function(){
        //   jQuery("main").load("pages-ipad/section3 .arabic",function(e){
        //       let section = $('section').attr("id")
        //     //   checkSection(section);
        //     });
        // });
      });
    }


    function section3(section) {
      console.log(section)
    }


    function getLottie(state){
        
        let svgContainer = document.getElementById('forStateSvg');
        let path2anim = './svg/states/';
        let lottieObj = {
          container: svgContainer,
          path: path2anim+state+'.json',
          renderer: 'svg',
          loop: false,
          autoplay: false,
        };

        return lottieObj;

    }

    function getIndustrialData(industrySelector) {
          
      $.ajax({
          type: "POST",
          url: '/fetchIndustrialData',
          data : { industryData : industrySelector },
          dataType: "json",
          processdata: true,

          success: function(re) {
            let forPopup = re.data;
            $('#mapPopup').html('').html(forPopup)
            $('#forSvg #mapPopup').fadeIn();
            $('#forSvg #mapPopup').css("z-index","2");
          },

          error: function() {

          }
        });
    }

    function section4(section) {
      console.log(section);
      //riyadh
      var riyadhAnim = lottie.loadAnimation(getLottie('riyadh'));
      var buraidahAnim = lottie.loadAnimation(getLottie('buraidah'));
      var hailAnim = lottie.loadAnimation(getLottie('hail'));
      var medinaAnim = lottie.loadAnimation(getLottie('medina'));
      var ararAnim = lottie.loadAnimation(getLottie('arar'));
      var skakaAnim = lottie.loadAnimation(getLottie('skaka'));
      var tabukAnim = lottie.loadAnimation(getLottie('tabuk'));
      var makkahAnim = lottie.loadAnimation(getLottie('makkah'));
      var albahaAnim = lottie.loadAnimation(getLottie('al_baha'));
      var abhaAnim = lottie.loadAnimation(getLottie('abha'));
      var jazanAnim = lottie.loadAnimation(getLottie('jazan'));
      var najranAnim = lottie.loadAnimation(getLottie('najran'));
      var dammanAnim = lottie.loadAnimation(getLottie('damman'));
      // ----------
      

      riyadhAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      buraidahAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      hailAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      medinaAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      ararAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      skakaAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      tabukAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      makkahAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      albahaAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      abhaAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      jazanAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      najranAnim.onComplete = function(e){
        sideBarFadeIn();
      }

      dammanAnim.onComplete = function(e){
        sideBarFadeIn();
      }


      riyadhAnim.addEventListener("DOMLoaded",function(){

        //ASSIGNING CITY TYPES

        $('.Riyadh1_1, .AlZulfi_21, .Sudayer_16, .Shaqra_32, .Dhurma_37, .Riyadh3_26, .AlKharj_17, .Riyadh2_2').addClass("industry");
        $('.RiyadhTechnology,.AlRaidah').addClass("technology");
        //$('.al, .sudair, .durma').addClass("modern");


        //$('.riyadh_2nd, .riyadh_3rd, .riyadh, .Shaqra_21, .al_kharj, .riyahd_1st, .al, .sudair, .durma').click(function(e){
        $('.Riyadh2_2, .Sudayer_16, .AlZulfi_21, .Shaqra_32, .Riyadh3_26, .Dhurma_37, .AlKharj_17, .Riyadh1_1').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        });

        $('.RiyadhTechnology').click(function(e){
          console.log($(this).attr("class").split(/\s+/)[0])
            $('#forSvg #riyadhPopup').fadeIn();
            $('#forSvg #riyadhPopup').css("z-index","2");
            e.stopPropagation();
        });
        
        $('.AlRaidah').click(function(e){
          console.log($(this).attr("class").split(/\s+/)[0])
            $('#forSvg #alraidahPopup').fadeIn();
            $('#forSvg #alraidahPopup').css("z-index","2");
            e.stopPropagation();
        });
      });

      buraidahAnim.addEventListener("DOMLoaded",function(){
        
        $('.AlQassim_7, .AlQassim2_25').addClass("industry");
        $('.AlQassimOsais_44').addClass("modern");

        $('.AlQassimOsais_44, .AlQassim_7, .AlQassim2_25').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      });

      hailAnim.addEventListener("DOMLoaded",function(){
        $('.Hail_13').addClass("industry");

        $('.Hail_13').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      medinaAnim.addEventListener("DOMLoaded",function(){
        
        $('.AlMadinaAlMunawwara_9').addClass("industry");
        $('.YanbueOsais_42').addClass("modern");

        $('.AlMadinaAlMunawwara_9, .YanbueOsais_42').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      ararAnim.addEventListener("DOMLoaded",function(){
            
        $('.Arar_19, .waadalshamal_48').addClass("industry");

        $('.Arar_19, .waadalshamal_48').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      skakaAnim.addEventListener("DOMLoaded",function(){
        $('.skaka12').addClass("industry");
        $('.AlJoufOsais_11').addClass("modern");
        //NO IDS in SVG
        $('.AlJoufOsais_11').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })

      })

      tabukAnim.addEventListener("DOMLoaded",function(){

        $('.Tabuk_12').addClass("industry");

        $('.Tabuk_12').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      makkahAnim.addEventListener("DOMLoaded",function(){
        
        $('.Rabigh_39, .Jeddah1_3, .Jeddah2_15, .Jeddah3_28, .MakkahAlMukarrama_6').addClass("industry");
        $('.WadiMakkah').addClass("technology");
        $('.JeddahOsais_29').addClass("modern");

        $('.Rabigh_39, .Jeddah1_3, .Jeddah2_15, .Jeddah3_28, .MakkahAlMukarrama_6, .JeddahOsais_29').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })

        $('.WadiMakkah').click(function(e){
          console.log($(this).attr("class").split(/\s+/)[0])
            $('#forSvg #wadiPopup').fadeIn();
            $('#forSvg #wadiPopup').css("z-index","2");
            e.stopPropagation();
        });
      })

      albahaAnim.addEventListener("DOMLoaded",function(){
        $('.Albaha_27').addClass("industry");

        $('.Albaha_27').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      abhaAnim.addEventListener("DOMLoaded",function(){
        
        $('.Assir_10').addClass("industry");

        $('.Assir_10').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      jazanAnim.addEventListener("DOMLoaded",function(){
        
        $('.Jazan_18').addClass("industry");

        $('.Jazan_18').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      najranAnim.addEventListener("DOMLoaded",function(){
        
        $('.Najran_14').addClass("industry");

        $('.Najran_14').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        })
      })

      dammanAnim.addEventListener("DOMLoaded",function(){

        $('.Alahsa_36, .Dammam_5, .Dammam1_4, .Dammam3_30, .Ahsa1_8, .HafrAlbatin_35').addClass("industry");
        $('.AhsaOsais_38').addClass("modern");


        $('.Alahsa_36, .Dammam_5, .Dammam1_4, .Dammam3_30, .Ahsa1_8, .HafrAlbatin_35, .AhsaOsais_38').click(function(e){
          var temp = $(this).attr("class").split(/\s+/)[0];
          console.log(temp)
          getIndustrialData(temp)
          e.stopPropagation();
        });
      })





   $('.checkbox input').on("change",function(){
      if($("#check1").is(':checked')){
      $.ajax({
      type: "GET",
      url: '/check1',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.industry').fadeIn(500);
      }
      });
      }else{
      $.ajax({
      type: "GET",
      url: '/uncheck1',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.industry').fadeOut(500);
      }
      });
      }
      if($("#check2").is(':checked')){
      $.ajax({
      type: "GET",
      url: '/check2',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.technology').fadeIn(500);
      }
      });
      }else{
      $.ajax({
      type: "GET",
      url: '/uncheck2',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.technology').fadeOut(500);
      }
      });
      }
      if($("#check3").is(':checked')){
      $.ajax({
      type: "GET",
      url: '/check3',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.modern').fadeIn(500);
      }
      });
      }else{
      $.ajax({
      type: "GET",
      url: '/uncheck3',
      dataType: "json",
      processdata: true,
      success: function(re) {
      $('.modern').fadeOut(500);
      }
      });
      }
      if(($("#check1").is(':checked')||$("#check2").is(':checked')||$("#check3").is(':checked'))==false){
      $('.modern, .industry, .technology').fadeOut(100);
      }
    })

      //CITY ANIMATIONS ON SAUDI MAP

      $('#Riyadh,[data-name=" Riyadh"]').on("click",function(){
        stateAnimate('.Riyadh',riyadhAnim);
        $('#section4 .map-head.city').html("Al Riyadh")
      })

      $('#Buraidah,[data-name="buraidah"]').on("click",function(){
        stateAnimate('.Buraidah',buraidahAnim);
        $('#section4 .map-head.city').html("Qassim")
      })

      $('#Hali,[data-name=" Hail"]').on("click",function(){
        stateAnimate('.Hail',hailAnim);
        $('#section4 .map-head.city').html("Hail")
      })

      $('#Medina,[data-name=" Medina"]').on("click",function(){
        stateAnimate('.Medina',medinaAnim);
        $('#section4 .map-head.city').html("Madinah")
      })

      $('#Arar,[data-name=" Arar"]').on("click",function(){
        stateAnimate('.Arar',ararAnim);
        $('#section4 .map-head.city').html("Northern")
      })

      $('#Skaka,[data-name=" Skaka"]').on("click",function(){
        stateAnimate('.Skaka1',skakaAnim);
        $('#section4 .map-head.city').html("Al Jawf")
      })

      $('#Tabuk,[data-name=" Tabuk"]').on("click",function(){
        stateAnimate('.Tabuk1',tabukAnim);
        $('#section4 .map-head.city').html("Tabuk")
      })

      $('#Makkah,[data-name=" Makkah"]').on("click",function(){
        stateAnimate('.Makkah1',makkahAnim);
        $('#section4 .map-head.city').html("Makkah")
      })

      $('#Al_Baha,[data-name=" Al_baha"]').on("click",function(){
        stateAnimate('.Al',albahaAnim);
        $('#section4 .map-head.city').html("Baha")
      })

      $('#Abha,[data-name="abha-2"]').on("click",function(){
        stateAnimate('.Abha',abhaAnim);
        $('#section4 .map-head.city').html("Asir")
      })

      $('#Jazan,[data-name=" Jazan"]').on("click",function(){
        stateAnimate('.Jazan1',jazanAnim);
        $('#section4 .map-head.city').html("Jazan")
      })

      $('#Najran,[data-name=" Najran"]').on("click",function(){
        stateAnimate('.Najran1',najranAnim);
        $('#section4 .map-head.city').html("Najran")
      })

      $('#Dammam,[data-name=" Damman"]').on("click",function(){
        stateAnimate('.Damman1a',dammanAnim);
        $('#section4 .map-head.city').html("Eastern");
      })

    }


    function section5(section) {
      console.log(section)
    }

    function section6(section) {
      console.log("inpad")
      // BAR CHART CODE
      var dataGraph = JSON.parse($('#targtdInd').val())
      var data = {
          labels: [],
          series: []
      };
      data.labels = dataGraph.labels
      data.series.push(dataGraph.series)
      console.log(data)
      var options = {
          fullWidth: true,
          seriesBarDistance: 15,
          axisX: {
              offset: 90
          },
          axisY: {
              onlyInteger: true
          }
      };
      new Chartist.Bar('.ct-chart', data, options);
      APP.makeCheckbox();

    }

    function section7(section) {
      console.log(section)
    }

    //COMMON SIDE BUTTONS FOR LOADING

    //reset button

    jQuery("main").on("click",".reset",function(){
        location.reload();
    });

    // ANIMATION FUNCTIONS

    function sideBarFadeIn(){
      $('nav').animate({"opacity":"1","z-index":"2"},200);
    }

    function stateAnimate(state,stateAnim){

      $.ajax({
        type: "POST",
        url: '/stateAnimate',
        data : { stname:state },
        dataType: "json",
        processdata: true,

        success: function(re) {

          jQuery('.map-head.region').hide();
          jQuery('.map-head.city').fadeIn();
          $('#forSvg #saudi').fadeOut(500);
          console.log("showing "+state)
          $(state).parent().parent().show();
          stateAnim.goToAndPlay(0,true);

        },

        error: function() {

        }
      });

    }

    function makePopup(){
    }
});
